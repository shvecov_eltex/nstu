import { IUniversity } from "./IUniversity"
import { Student } from "./Student"
import { Teacher } from "./Teacher"
import { IPerson } from "./IPerson"

export class University implements IUniversity
{
	persons: IPerson[];
    students: Student[];
    teachers: Teacher[];

	constructor(iuniversity: IUniversity)
	{
		this.persons = this.sort_lastname(iuniversity.persons);
		this.students = <Student[]>this.sort_lastname(iuniversity.students);
		this.teachers = <Teacher[]>this.sort_lastname(iuniversity.teachers);
	}

	FindByLastName(lastName: string): IPerson[]
	{
		let pers: IPerson[] = [];
		return pers;
	}

	private sort_lastname(persons: IPerson[]): IPerson[]
	{
		persons.sort((a, b) =>
		{
			if (a.LastName < b.LastName) return 1;
			if (a.LastName > b.LastName) return -1;
		})
		return persons;
	}
}