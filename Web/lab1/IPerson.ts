export interface IPerson
{ 
    Name: string;
    Patronomic: string; 
    LastName: string;

    BithDate?: Date;
    Weight?: number;
}
