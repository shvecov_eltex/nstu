import { IPerson } from "./IPerson";

enum Post
{
    _Teacher,
    _Intern,
    _Professor,
    _Docent,
    _Rector,
    _Dean
}

export class Teacher implements IPerson
{
    Name: string;
    Patronomic: string;
    LastName: string;
    Pulpit: string;
    Background: number;
    Post: Post;

    BithDate?: Date;
    Weight?: number;

    constructor(iperson: IPerson, pulpit: string, background: number, post: Post)
    {
        this.Name = iperson.Name;
        this.Patronomic = iperson.Patronomic;
        this.LastName = iperson.LastName;
        this.Pulpit = pulpit;
        this.Background = background;
        this.Post = post;

        this.BithDate = iperson.BithDate;
        this.Weight = iperson.Weight; 
    }
}
