import { IPerson } from "./IPerson"
import { Student } from "./Student"
import { Teacher } from "./Teacher"

export interface IUniversity { 

    persons: IPerson[];
    students: Student[];
    teachers: Teacher[];

    FindByLastName(lastName: string): IPerson[];
}
