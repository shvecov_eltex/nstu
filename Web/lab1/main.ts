import { promises as fsPromises } from "fs";
import { IUniversity } from "./IUniversity";
import { IPerson } from "./IPerson";
import { Student } from "./Student";

async function main()
{
	let iuniversity: IUniversity = {} as IUniversity;

	iuniversity.students = <Student[]>JSON.parse(await fsPromises.readFile("json/students.json", "utf8"));

	console.log("hello");
}

main();
