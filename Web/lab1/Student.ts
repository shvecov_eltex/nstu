import { IPerson } from "./IPerson"

export class Student implements IPerson
{
    Name: string;
    Patronomic: string;
    LastName: string;
    Course: number;
    Group: string;
    Average_point: number;

    BithDate?: Date;
    Weight?: number;

    constructor(iperson: IPerson, course: number, group: string, average: number)
    {
        this.Name = iperson.Name;
        this.Patronomic  = iperson.Patronomic;
        this.LastName = iperson.LastName;
        this.Course = course;
        this.Group = group;
        this.Average_point = average;

        this.BithDate = iperson.BithDate;
        this.Weight = iperson.Weight;
    }
}
